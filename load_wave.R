libr"load_wave" <- function(filename){
  mywave <- load.wave(filename);
  mywave <- as.numeric(mywave);
  
  return(mywave);
}
  