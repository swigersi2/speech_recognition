library(shiny)

shinyUI(fluidPage(
  
  titlePanel("HouseMaid"),
  
  sidebarLayout(
    sidebarPanel(
      actionButton("button1","Record"),
      uiOutput("uiButton2"),
      img(src="Logo5.png", height = 200, width=200)
    ),
    
    mainPanel(
      textOutput("textToPrint")
    )
  )
))