"voice_activation" <- function(signal,threshold,vote){
  
  #Execute voice activation algorithm (on energy basis)
  
  ##Simple algoritm starts here##
  
  act_sig <- c();
  new_sig <- c();
  vote_range <- 20;
  
  #Recognize actual signal sensing energy
  for(i in 1:length(signal)){
    ifelse((signal[i])^2 < threshold, act_sig[i] <- 0, act_sig[i] <- signal[i]);
  }
  act_sig <- 20*act_sig;
  
  ##Simple algorith ends here - everything below is my thoughts and ideas##
  
  #Search for empty fragments using voting system
  for(i in 1:length(act_sig)){
    ifelse(i-vote_range < 1, begin <- 1, begin <- i-vote_range);
    ifelse(i+vote_range > length(signal), end <- length(signal), end <- i +vote_range);
    
    num_of_0 <- length(subset(act_sig[begin:end],act_sig[begin:end]==0));
    ifelse(num_of_0/length(act_sig[begin:end]) > vote, new_sig[i] <- NA, new_sig[i] <- act_sig[i]);
  }
  
  #Cut selected fragments of signal
  new_sig <- new_sig[!is.na(new_sig)];
  
  #Save it as 'new_sig.wav'
  savewav(new_sig,f=16000);
}